# Stomachs
## Context

<p style="text-align: justify"> The first mention of plastic ingestion is from Kenyon and Kridler (1969). However, the ingestion of debris has been known long before that, as Couch reported the ingestion of a candle in an Wilson's storm-petrel in 1838 (<em>Oceanites oceanicus</em>) (<em>Provencher et al., 2017</em>).</p>
<p style="text-align: justify"> Marine birds primarily feed on marine organisms (<em>Oro, Martínez-Abrain, 2015</em>). The most common explanation for the ingestion of plastics is that there is confusion between fragments and prey (<em>Santos et al., 2016</em>). Indeed, color can be considered an attracting factor for predators. In particular when these colors resemble those specific to their prey (<em>Bergmann et al., 2015</em>). The ingestion of these plastics leads to their accumulation in the stomachs of birds, which is a major problem at the individual and subindividual levels (<em>Rochman et al., 2016; Van Franeker, Meijboom, 2002</em>). There is very little knowledge about the importance of color for the ingestion of plastics by marine birds (<em>Lavers, Bond, 2016</em>).</p>
<p style="text-align: justify"> Observing the frequency of interaction between marine birds and plastic debris can provide an indication of pollution from these materials (<em>Robards et al., 1995; Bond et al., 2012</em>). Indeed, it is possible that the prevalence of transparent or light-colored plastic pieces in the stomachs of marine birds reflects their prevalence in the environment more than their selection. </p>
<p style="text-align: justify"> Birds have a very different vision than ours, indeed they have a tetrachromatic vision (<em>Bowmaker, 1980</em>), they therefore have four types of cones (<em>Verlis et al., 2013</em>), they thus have access to colors that the human eye can not perceive, because human vision is limited to three colors: red, green and blue (<em>Bowmaker, 1980</em>). Birds are sensitive to these colors too, but they are also able to perceive short wavelength waves, whether violet or ultraviolet. Among marine birds only gulls and a few species of terns are sensitive to ultraviolet, the vast majority have cones sensitive to violet (<em>Machovsky Capuska et al., 2011</em>). The experience of color is subjective (<em>Maia, White, 2018</em>) so we need to find an objective, reliable, and accessible way to classify plastics by their colors. That's why use of photography and this protocol could help many scientists </p>



## General description

« Stomachs » is a web application that aimed to list plastics in bird stomachs by their colours and location. The goal is to allow scientists working on the ingestion of plastics by birds to share data on a collaborative platform, in order to study and monitor the plastics ingested worldwide. 

The application will provide an internal tool to perform the measurements on the photographs, which will only require a little material in order to compare the results with each other. 
- A light box 
- A camera that can record in RAW format (i.e. *.CR2) 
- A white balance card 
- Software for white balance (future functionality of the application)

### Vocabulary : 
__Species___ :  a Species  i.e “_Fratercula arctica_”  
__Location__ : is the harvest location of sample  
__Study__ : is a group for multiple samples could be linked to a published study  
__Sample__ : is a sample considered as one bird gizzards   
__Plastic__ : is a single plastic linked to sample and 3 colour measurement

## API :

### Species
| URL | Methods | Description | Parameters |  
| :---------- | :----------:| :------------- | :------ |
| v1/species | GET | return all species| - |
| v1/species/id | GET | return selected species | id of the selected species |
| v1/species/count | GET | return total number of species | - |

### Locations
| URL | Methods | Description | Parameters |  
| :---------- | :----------:| :------------- | :------ |
| v1/locations | GET | return all locations| - |
| v1/locations/id | GET | return selected location | id of the selected location |
| v1/locations| POST | create a new location| takes an body as parameter with the location|
| v1/locations| PUT | create or update a location | takes a body as parameter with the location|

### Studies
| URL | Methods | Description | Parameters |  
| :---------- | :----------:| :------------- | :------ |
| v1/studies | GET | return all studies| - |
| v1/studies/id | GET | return selected study | id of the selected study |


## Concept views (WIP)
This section presents prototypes of parts of the final application in order to show what it might look like and to describe in more detail the different features.  
**These concepts are not intended to represent the final application but just to describe what it might look like.**

### Species cards
Each species will have a main data sheet available, with information displayed such as a graph showing the distribution of plastics by color and degree of clarity (Value of HSV), the number by locality

![img.png](images/img.png)

### Sample cards

![sample_card.jpg](images/sample_card.jpg)

