package springConfiguration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;


@SpringBootApplication
@ComponentScan(basePackages = {"api", "services", "core", "infra"})
@EntityScan(basePackages = "infra.databaseDtos")
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
