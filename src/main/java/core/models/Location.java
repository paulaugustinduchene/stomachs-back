package core.models;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class Location {

	private Integer locationID;
	private String locationName; 
	private Double locationLat;
	private Double locationLong;

	public Integer getLocationID() {
		return locationID;
	}
	public String getLocationName() {
		return locationName;
	}
	public Double getLocationLat() {
		return locationLat;
	}
	public Double getLocationLong() {
		return locationLong;
	}

	public void setLocationID(Integer locationID) {
		this.locationID = locationID;
	}
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}
	public void setLocationLat(Double locationLat) {
		this.locationLat = locationLat;
	}
	public void setLocationLong(Double locationLong) {
		this.locationLong = locationLong;
	}

	@Override
	public boolean equals(Object o) {
		return EqualsBuilder.reflectionEquals(this, o);
	}

	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
