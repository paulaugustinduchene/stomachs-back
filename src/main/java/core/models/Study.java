package core.models;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class Study {
	
	private Integer studyId;
	private String studyName;
	private String studyDoi;

	public Integer getStudyId() {
		return studyId;
	}
	
	public String getStudyName() {
		return studyName;
	}
	
	public String getStudyDoi() {
		return studyDoi;
	}
	
	public void setStudyId(Integer studyId) {
		this.studyId = studyId;
	}
	
	public void setStudyName(String studyName) {
		this.studyName = studyName;
	}
	
	public void setStudyDoi(String studyDoi) {
		this.studyDoi = studyDoi;
	}


	@Override
	public boolean equals(Object o) {
		return EqualsBuilder.reflectionEquals(this, o);
	}

	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
	
	

}
