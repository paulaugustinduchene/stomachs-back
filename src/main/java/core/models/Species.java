package core.models;


import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class Species {
    private Integer speciesID;
    private String commonName;
    private String kindName;
    private String speciesName;
    private String familyName;
    private String status;

    public Integer getSpeciesID() {
        return speciesID;
    }
    public String getCommonName() {
        return commonName;
    }
    public String getKindName() {
        return kindName;
    }
    public String getSpeciesName() {
        return speciesName;
    }
    public String getFamilyName() {
        return familyName;
    }
    public String getStatus() {
        return status;
    }

    public void setSpeciesID(Integer speciesID) {
        this.speciesID = speciesID;
    }
    public void setCommonName(String commonName) {
        this.commonName = commonName;
    }
    public void setKindName(String kindName) {
        this.kindName = kindName;
    }
    public void setSpeciesName(String speciesName) {
        this.speciesName = speciesName;
    }
    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }
    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        return EqualsBuilder.reflectionEquals(this, o);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
