package core.models;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.time.LocalDate;

public class Sample {
	
	private Integer sampleID;
	private LocalDate sampleDate;
	private Integer sampleStudy;
	private Integer sampleSpecies;
	private Integer sampleLocation;

	public Integer getSampleID() {
		return sampleID;
	}

	public void setSampleID(Integer sampleID) {
		this.sampleID = sampleID;
	}

	public LocalDate getSampleDate() {
		return sampleDate;
	}

	public void setSampleDate(LocalDate sampleDate) {
		this.sampleDate = sampleDate;
	}

	public Integer getSampleStudy() {
		return sampleStudy;
	}

	public void setSampleStudy(Integer sampleStudy) {
		this.sampleStudy = sampleStudy;
	}

	public Integer getSampleSpecies() {
		return sampleSpecies;
	}

	public void setSampleSpecies(Integer sampleSpecies) {
		this.sampleSpecies = sampleSpecies;
	}

	public Integer getSampleLocation() {
		return sampleLocation;
	}

	public void setSampleLocation(Integer sampleLocation) {
		this.sampleLocation = sampleLocation;
	}

	@Override
	public boolean equals(Object o) {
		return EqualsBuilder.reflectionEquals(this, o);
	}

	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
