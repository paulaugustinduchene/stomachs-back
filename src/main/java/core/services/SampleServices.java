package core.services;

import java.util.List;

import core.models.Sample;
import exceptions.CustomMapperException;
import exceptions.SampleNotFoundException;
import infra.repository.SampleRepository;
import org.springframework.stereotype.Service;

@Service
public class SampleServices {

	private SampleRepository sampleRepository;

	public SampleServices(SampleRepository sampleRepository) {
		this.sampleRepository = sampleRepository;
	}

	public List<Sample> getAllSamples() {
		return sampleRepository.listAll();
	}

	public Sample getSampleById(Integer id) throws SampleNotFoundException {
		return sampleRepository.getById(id);
	}

	public Sample createSample(Sample sample) throws CustomMapperException {
		return sampleRepository.createSample(sample);
	}

	public void updateSample(Long id, Sample sample) {
		//TODO :
	}

	public void deleteSample(Long id) {
		// TODO :
	}
}

