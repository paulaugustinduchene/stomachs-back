package core.services;

import java.util.List;

import core.models.Study;
import exceptions.CustomMapperException;
import exceptions.StudyNotFoundException;
import infra.repository.StudyRepository;
import org.springframework.stereotype.Service;


@Service
public class StudyServices {

	private StudyRepository studyRepository;

	public StudyServices(StudyRepository studyRepository) {
		this.studyRepository = studyRepository;
	}

	public List<Study> listStudies(){
		return studyRepository.listAllStudies();
	}

	public Study getStudyById(Integer id) throws StudyNotFoundException {
		return studyRepository.getStudyById(id);
	}

    public Study createStudy(Study study) throws CustomMapperException {
		return studyRepository.create(study);
    }

    public void delete(Integer id) throws StudyNotFoundException {
		studyRepository.delete(id);
    }

	public Study updateStudy(Integer id, Study study) throws CustomMapperException {
		study.setStudyId(id);
		return studyRepository.update(study);
	}
}
