package core.services;

import core.models.Species;
import exceptions.SpeciesNotFoundException;
import infra.repository.SpeciesRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SpeciesServices {

    private SpeciesRepository speciesRepository;

    public SpeciesServices(SpeciesRepository speciesRepository) {
        this.speciesRepository = speciesRepository;
    }

    public List<Species> getSpeciesList(){
        List<Species> species = speciesRepository.listAll();
        return species;
    }

    public Species getSpeciesById(Integer id) throws SpeciesNotFoundException {
        Species species = speciesRepository.getById(id);
        return species;
    }

    public Long speciesCount() {
        return speciesRepository.count();
    }


}
