package core.services;

import java.util.List;

import core.models.Location;
import exceptions.CustomMapperException;
import exceptions.LocationNotFoundException;
import infra.repository.LocationRepository;
import org.springframework.stereotype.Service;

@Service
public class LocationServices {

	private LocationRepository locationRepository;

	public LocationServices(LocationRepository locationDao) {
		this.locationRepository = locationDao;
	}

	public List<Location> getLocationList(){
		List<Location> location = locationRepository.listAll();
		return location;
	}

	public Location createLocation(Location location) throws CustomMapperException {
		return locationRepository.addLocation(location);
	}

	public Location getLocationById(Integer id) throws LocationNotFoundException {
		return locationRepository.getLocationById(id);
	}

	public Location updateLocation(Location location) throws CustomMapperException {
		return locationRepository.update(location);
	}

	public void deleteLocationById(Integer id) throws LocationNotFoundException {
		locationRepository.delete(id);
	}
}
