package exceptions;

public class SpeciesNotFoundException extends Exception{

    private String message;

    public SpeciesNotFoundException() {
            super();
    }

    public SpeciesNotFoundException(String message) {
        super(message);

    }

    public String getMessage() {

        return message;

    }


}
