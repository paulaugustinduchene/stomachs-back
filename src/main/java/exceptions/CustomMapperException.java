package exceptions;

public class CustomMapperException extends Exception {

    public CustomMapperException() {
    }

    public CustomMapperException(String message) {
        super(message);
    }
}
