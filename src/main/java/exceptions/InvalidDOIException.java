package exceptions;

public class InvalidDOIException extends Exception {

    public InvalidDOIException() {
    }

    public InvalidDOIException(String message) {
        super(message);
    }
}
