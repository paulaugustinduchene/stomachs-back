package exceptions;

public class SampleNotFoundException extends Exception{
    public SampleNotFoundException() {
    }

    public SampleNotFoundException(String message) {
        super(message);
    }
}
