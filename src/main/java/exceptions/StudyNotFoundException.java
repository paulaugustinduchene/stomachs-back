package exceptions;

public class StudyNotFoundException extends Exception{

    public StudyNotFoundException() {
    }

    public StudyNotFoundException(String message) {
        super(message);
    }
}
