package infra.repository;

import core.models.Species;
import exceptions.SpeciesNotFoundException;
import infra.databaseDtos.SpeciesDatabaseDto;
import infra.mappers.SpeciesDtoMapper;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;

@Repository
public class SpeciesRepository {

    @PersistenceContext
    private EntityManager entityManager;

    private SpeciesDtoMapper speciesDtoMapper;

    private static final String GET_ALL_SPECIES = "SELECT speciesList FROM SpeciesDatabaseDto speciesList";
    private static final String COUNT_SPECIES = "SELECT COUNT(speciesList) FROM SpeciesDatabaseDto speciesList";

    public SpeciesRepository(EntityManager entityManager, SpeciesDtoMapper speciesDtoMapper) {
        this.entityManager = entityManager;
        this.speciesDtoMapper = speciesDtoMapper;
    }

    public List<Species> listAll() {
        List<SpeciesDatabaseDto> species;
        try(Session session = entityManager.unwrap(Session.class)){
            Query query = session.createQuery(GET_ALL_SPECIES);
             species = query.getResultList();
        }

        return speciesDtoMapper.speciesDtosToSpecies(species);
    }

    public Species getById(Integer id) throws SpeciesNotFoundException {
        SpeciesDatabaseDto species;
        try(Session session = entityManager.unwrap(Session.class)){
            species =  session.get(SpeciesDatabaseDto.class, id);
            return speciesDtoMapper.specieDtoToSpecies(species).orElseThrow(SpeciesNotFoundException::new);
        }
    }

    @Transactional
    public Long count() {
        Long count;
        try(Session session = entityManager.unwrap(Session.class)){
            TypedQuery<Long> query = session.createQuery(COUNT_SPECIES);
            count = query.getSingleResult();
        }
        System.out.println(count);
        return count;
    }
}
