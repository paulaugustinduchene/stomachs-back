package infra.repository;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import core.models.Sample;
import exceptions.CustomMapperException;
import exceptions.SampleNotFoundException;
import infra.databaseDtos.SampleDatabaseDto;
import infra.mappers.SampleDtoMapper;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;


@Repository
public class SampleRepository {


	@PersistenceContext
	private EntityManager entityManager;
	private SampleDtoMapper sampleDtoMapper;

	private static final String GET_ALL_SAMPLES = "SELECT samples FROM SampleDatabaseDto samples";
	private static final String GET_SAMPLE_BY_ID = "FROM SampleDatabaseDto samples WHERE sample_id = :sample_id";


	public SampleRepository(EntityManager entityManager, SampleDtoMapper sampleDtoMapper) {
		this.entityManager = entityManager;
		this.sampleDtoMapper = sampleDtoMapper;
	}

	public List<Sample> listAll() {
		try(Session session = entityManager.unwrap(Session.class)){
			Query query = session.createQuery(GET_ALL_SAMPLES, SampleDatabaseDto.class);
			List<SampleDatabaseDto> sampleDatabaseDtos = query.getResultList();
			List<Sample> samples = sampleDtoMapper.dtosToSamples(sampleDatabaseDtos);
			return samples;
		}

	}

	public Sample getById(Integer id) throws SampleNotFoundException {
		try(Session session = entityManager.unwrap(Session.class)){
			Query query = session.createQuery(GET_SAMPLE_BY_ID, SampleDatabaseDto.class);
			query.setParameter("sample_id",id);
			SampleDatabaseDto sampleDatabaseDto = (SampleDatabaseDto) query.getSingleResult();
			return sampleDtoMapper.dtoToSample(sampleDatabaseDto).orElseThrow(SampleNotFoundException::new);
		}
	}

	@Transactional
	public Sample createSample(Sample sample) throws CustomMapperException {

		Optional<SampleDatabaseDto> sampleDatabaseDto = sampleDtoMapper.sampleToSampleDto(sample);

		if(sampleDatabaseDto.isPresent()) {
			SampleDatabaseDto sampleDto = sampleDatabaseDto.get();
			try (Session session = entityManager.unwrap(Session.class)) {
				sampleDto.setSampleID((Integer) session.save(sampleDto));
				return sampleDtoMapper.dtoToSample(sampleDto).orElseThrow(CustomMapperException::new);
			}
		} else {
			throw new CustomMapperException();
		}
	}

	public void delete() {
		// TODO Auto-generated method stub
	}

	public void update() {
		// TODO Auto-generated method stub
		
	}


}
