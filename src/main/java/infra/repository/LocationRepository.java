package infra.repository;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import core.models.Location;
import exceptions.CustomMapperException;
import exceptions.LocationNotFoundException;
import infra.databaseDtos.LocationDatabaseDto;
import infra.mappers.LocationDtoMapper;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


@Repository
public class LocationRepository {

	@PersistenceContext
	private EntityManager entityManager;

	private LocationDtoMapper locationDtoMapper;

	private static final String GET_ALL_LOCATIONS = "SELECT locations FROM LocationDatabaseDto locations";
	private static final String GET_LOCATION_BY_ID = "FROM LocationDatabaseDto locations WHERE location_id = :location_id";


	public LocationRepository(EntityManager entityManager, LocationDtoMapper locationDtoMapper) {
		this.entityManager = entityManager;
		this.locationDtoMapper= locationDtoMapper;
	}

	public List<Location> listAll(){
		List<LocationDatabaseDto> locations;
		try(Session session = entityManager.unwrap(Session.class)){
			Query query = session.createQuery(GET_ALL_LOCATIONS);
			locations = query.getResultList();
		}
		return locationDtoMapper.dtosToLocations(locations);
	}

	@Transactional
	public Location addLocation(Location location) throws CustomMapperException {

		Optional<LocationDatabaseDto> locationOptional = locationDtoMapper.locationToDto(location);

		if(locationOptional.isPresent()){
			LocationDatabaseDto locationDatabaseDto = locationOptional.get();

			try(Session session = entityManager.unwrap(Session.class)){
				locationDatabaseDto.setLocationID((Integer) session.save(locationDatabaseDto));
			}

			return locationDtoMapper.dtoToToLocation(locationDatabaseDto).orElseThrow(CustomMapperException::new);
		} else {
			throw new CustomMapperException();
		}
	}

	public Location getLocationById(Integer id) throws LocationNotFoundException {
		LocationDatabaseDto locationDto;
		try(Session session = entityManager.unwrap(Session.class)){
			locationDto =  session.get(LocationDatabaseDto.class, id);
			return locationDtoMapper.dtoToToLocation(locationDto).orElseThrow(LocationNotFoundException::new);
		}
	}

	@Transactional
	public Location update(Location location) throws CustomMapperException {
		try(Session session = entityManager.unwrap(Session.class)){
			LocationDatabaseDto locationDto = (LocationDatabaseDto) session.merge(locationDtoMapper.locationToDto(location).orElseThrow(CustomMapperException::new));
			return locationDtoMapper.dtoToToLocation(locationDto).orElseThrow(CustomMapperException::new);
		}
	}

	@Transactional
	public void delete(Integer id) throws LocationNotFoundException {
		try (Session session = entityManager.unwrap(Session.class)) {
			Query query = session.createQuery(GET_LOCATION_BY_ID, LocationDatabaseDto.class);
			query.setParameter("location_id",id);
			LocationDatabaseDto locationDatabaseDto = (LocationDatabaseDto) query.getSingleResult();
			session.delete(locationDatabaseDto);
		}
	}
}
