package infra.repository;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import core.models.Study;
import exceptions.CustomMapperException;
import exceptions.StudyNotFoundException;
import infra.databaseDtos.StudyDatabaseDto;
import infra.mappers.StudyDtoMapper;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


@Repository
public class StudyRepository {

	private static final String GET_ALL_STUDIES = "SELECT studies FROM StudyDatabaseDto studies";

	@PersistenceContext
	private EntityManager entityManager;

	private StudyDtoMapper studyDtoMapper;

	public StudyRepository(EntityManager entityManager, StudyDtoMapper studyDtoMapper) {
		this.entityManager = entityManager;
		this.studyDtoMapper = studyDtoMapper;
	}

	public List<Study> listAllStudies() {
		List<StudyDatabaseDto> studyDatabaseDto;
		try(Session session = entityManager.unwrap(Session.class) ){
			Query query = session.createQuery(GET_ALL_STUDIES);
			studyDatabaseDto = query.getResultList();
		}
		return studyDtoMapper.dtosToStudies(studyDatabaseDto);
	}

	public Study getStudyById(Integer id) throws StudyNotFoundException {
		StudyDatabaseDto studyDatabaseDto;
		try(Session session = entityManager.unwrap(Session.class)){
			studyDatabaseDto =  session.get(StudyDatabaseDto.class, id);
			return studyDtoMapper.dtoToStudy(studyDatabaseDto).orElseThrow(StudyNotFoundException::new);
		}
	}

	@Transactional
	public Study create(Study study) throws CustomMapperException {

		Optional<StudyDatabaseDto> studyOptional = studyDtoMapper.studyToDto(study);

		if(studyOptional.isPresent()){
			StudyDatabaseDto studyDatabaseDto = studyOptional.get();

			try(Session session = entityManager.unwrap(Session.class)){
				studyDatabaseDto.setStudyId((Integer) session.save(studyDatabaseDto));
				return studyDtoMapper.dtoToStudy(studyDatabaseDto).orElseThrow(CustomMapperException::new);
			}
		} else {
			throw new CustomMapperException();
		}

	}

	public Study update(Study study) throws CustomMapperException {

		Optional<StudyDatabaseDto> studyDatabaseDto = studyDtoMapper.studyToDto(study);

		if(studyDatabaseDto.isPresent()){
			try(Session session = entityManager.unwrap(Session.class)){
				session.saveOrUpdate(studyDatabaseDto.get());
				return studyDtoMapper.dtoToStudy(studyDatabaseDto.get()).orElseThrow(CustomMapperException::new);
			}
		} else {
			throw new CustomMapperException();
		}
	}

	public void delete(Integer id) throws StudyNotFoundException {
		try(Session session = entityManager.unwrap(Session.class)){
			StudyDatabaseDto studyDatabaseDto = session.get(StudyDatabaseDto.class, id);
			if(studyDatabaseDto != null){
				session.delete(studyDatabaseDto);
			}else{
				throw new StudyNotFoundException();
			}
		}
	}
}
