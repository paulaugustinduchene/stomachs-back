package infra.databaseDtos;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.time.LocalDate;

import javax.persistence.*;

@Entity
@Table(name="samples")
public class SampleDatabaseDto {
	
	@Id
	@Column(name="sample_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer sampleID;

	@Column(name="sample_date")
	private LocalDate sampleDate;

	@JoinColumn(name="study_id")
	@ManyToOne
	private StudyDatabaseDto studyId;

	@JoinColumn(name="species_id")
	@ManyToOne
	private SpeciesDatabaseDto speciesId;

	@JoinColumn(name="location_id", referencedColumnName = "location_id")
	@ManyToOne
	private LocationDatabaseDto locationId;
	
	public SampleDatabaseDto() {

	}

	public Integer getSampleID() {
		return sampleID;
	}

	public void setSampleID(Integer sampleID) {
		this.sampleID = sampleID;
	}

	public LocalDate getSampleDate() {
		return sampleDate;
	}

	public void setSampleDate(LocalDate sampleDate) {
		this.sampleDate = sampleDate;
	}

	public StudyDatabaseDto getStudyId() {
		return studyId;
	}

	public void setStudyId(StudyDatabaseDto studyId) {
		this.studyId = studyId;
	}

	public SpeciesDatabaseDto getSpeciesId() {
		return speciesId;
	}

	public void setSpeciesId(SpeciesDatabaseDto speciesId) {
		this.speciesId = speciesId;
	}

	public LocationDatabaseDto getLocationId() {
		return locationId;
	}

	public void setLocationId(LocationDatabaseDto locationId) {
		this.locationId = locationId;
	}

	@Override
	public boolean equals(Object o) {
		return EqualsBuilder.reflectionEquals(this, o);
	}

	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
