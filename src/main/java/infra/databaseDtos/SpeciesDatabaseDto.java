package infra.databaseDtos;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.persistence.*;

@Entity
@Table(name = "species")
public class SpeciesDatabaseDto {

    @Id
    @Column(name = "species_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int speciesID;

    @Column(name = "common_name")
    private String commonName;

    @Column(name = "kind_name")
    private String kindName;

    @Column(name = "species_name")
    private String speciesName;

    @Column(name = "family_name")
    private String familyName;

    @Column(name = "status")
    private String status;


    public SpeciesDatabaseDto() {
        super();
    }

    public int getSpeciesID() {
        return speciesID;
    }

    public String getCommonName() {
        return commonName;
    }

    public String getKindName() {
        return kindName;
    }

    public String getSpeciesName() {
        return speciesName;
    }

    public String getFamilyName() {
        return familyName;
    }

    public String getStatus() {
        return status;
    }


    public void setSpeciesID(int speciesID) {
        this.speciesID = speciesID;
    }

    public void setCommonName(String commonName) {
        this.commonName = commonName;
    }

    public void setKindName(String kindName) {
        this.kindName = kindName;
    }

    public void setSpeciesName(String speciesName) {
        this.speciesName = speciesName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        return EqualsBuilder.reflectionEquals(this, o);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
