package infra.databaseDtos;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "studies")
public class StudyDatabaseDto {

	@Id
	@Column(name = "study_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer studyId;

	@Column(name = "study_name")
	private String studyName;

	@Column(name = "study_doi")
	private String studyDoi;


	public Integer getStudyId() {
		return studyId;
	}

	public String getStudyName() {
		return studyName;
	}

	public String getStudyDoi() {
		return studyDoi;
	}


	public void setStudyId(Integer studyId) {
		this.studyId = studyId;
	}

	public void setStudyName(String studyName) {
		this.studyName = studyName;
	}

	public void setStudyDoi(String studyDoi) {
		this.studyDoi = studyDoi;
	}


	@Override
	public boolean equals(Object o) {
		return EqualsBuilder.reflectionEquals(this, o);
	}

	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
