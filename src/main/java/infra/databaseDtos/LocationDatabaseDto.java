package infra.databaseDtos;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name ="locations")
public class LocationDatabaseDto {

	@Id
	@Column(name = "location_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int locationID;
	@Column(name = "location_name")
	private String locationName;
	@Column(name = "location_lat")
	private Double locationLat;
	@Column(name = "location_long")
	private Double locationLong;
	
	public LocationDatabaseDto() {
		super();
	}
	
	public int getLocationID() {
		return locationID;
	}

	public String getLocationName() {
		return locationName;
	}

	public double getLocationLat() {
		return locationLat;
	}

	public double getLocationLong() {
		return locationLong;
	}

	public void setLocationID(int locationID) {
		this.locationID = locationID;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public void setLocationLat(double locationLat) {
		this.locationLat = locationLat;
	}

	public void setLocationLong(double locationLong) {
		this.locationLong = locationLong;
	}

	@Override
	public boolean equals(Object o) {
		return EqualsBuilder.reflectionEquals(this, o);
	}

	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
