package infra.databaseDtos;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.persistence.*;

@Entity
@Table(name = "plastics")
public class PlasticDatabaseDto {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "plastic_id")
	private int plasticID;

	@JoinColumn(name="sample_id")
	@ManyToOne
	private SampleDatabaseDto sample;

	@Column(name = "color1")
	private String colorDatabaseDto1;

	@Column(name = "color2")
	private String colorDatabaseDto2;

	@Column(name = "color3")
	private String colorDatabaseDto3;

	public PlasticDatabaseDto() {
		super();
	}

	public int getPlasticID() {
		return plasticID;
	}

	public SampleDatabaseDto getSample() {
		return sample;
	}

	public String getColor1() {
		return colorDatabaseDto1;
	}

	public String getColor2() {
		return colorDatabaseDto2;
	}

	public String getColor3() {
		return colorDatabaseDto3;
	}

	public void setPlasticID(int plasticID) {
		this.plasticID = plasticID;
	}

	public void setSampleId(SampleDatabaseDto sample) {
		this.sample = sample;
	}

	public void setColorDatabaseDto1(String colorDatabaseDto1) {
		this.colorDatabaseDto1 = colorDatabaseDto1;
	}

	public void setColorDatabaseDto2(String colorDatabaseDto2) {
		this.colorDatabaseDto2 = colorDatabaseDto2;
	}

	public void setColorDatabaseDto3(String colorDatabaseDto3) {
		this.colorDatabaseDto3 = colorDatabaseDto3;
	}

	@Override
	public boolean equals(Object o) {
		return EqualsBuilder.reflectionEquals(this, o);
	}

	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
