package infra.mappers;

import core.models.Location;
import infra.databaseDtos.LocationDatabaseDto;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class LocationDtoMapper {

	public LocationDtoMapper() {

	}

	public Optional<LocationDatabaseDto> locationToDto(Location location){

		if(location == null){
			return Optional.empty();
		}

		LocationDatabaseDto locationDto =  new LocationDatabaseDto();
		locationDto.setLocationName(location.getLocationName());
		locationDto.setLocationLat(location.getLocationLat());
		locationDto.setLocationLong(location.getLocationLong());

		return Optional.of(locationDto);
	}

	public Optional<Location> dtoToToLocation(LocationDatabaseDto locationDto) {

		if(locationDto == null){
			return Optional.empty();
		}
		Location location = new Location();
		location.setLocationID(locationDto.getLocationID());
		location.setLocationName(locationDto.getLocationName());
		location.setLocationLat(locationDto.getLocationLat());
		location.setLocationLong(locationDto.getLocationLong());

		return Optional.of(location);
	}

	public List<Location> dtosToLocations(List<LocationDatabaseDto> locations) {

		if(locations == null){
			return new ArrayList<>();
		}

		return locations.stream()
				.map(location -> dtoToToLocation(location).orElse(null))
				.filter(Objects::nonNull)
				.collect(Collectors.toList());
	}
}
