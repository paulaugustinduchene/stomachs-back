package infra.mappers;

import core.models.Species;
import infra.databaseDtos.SpeciesDatabaseDto;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class SpeciesDtoMapper {

    public SpeciesDtoMapper() {

    }

    public static Optional<Species> specieDtoToSpecies(SpeciesDatabaseDto speciesDatabaseDto) {

        if(speciesDatabaseDto == null){
            return Optional.empty();
        }

        Species species = new Species();
        species.setSpeciesID(speciesDatabaseDto.getSpeciesID());
        species.setSpeciesName(speciesDatabaseDto.getSpeciesName());
        species.setCommonName(speciesDatabaseDto.getCommonName());
        species.setKindName(speciesDatabaseDto.getKindName());
        species.setFamilyName(speciesDatabaseDto.getFamilyName());
        species.setStatus(speciesDatabaseDto.getStatus());
        return Optional.of(species);
    }

    public static List<Species> speciesDtosToSpecies(List<SpeciesDatabaseDto> speciesDatabaseDtoList) {

        return speciesDatabaseDtoList.stream()
                .map(speciesDatabaseDto -> SpeciesDtoMapper.specieDtoToSpecies(speciesDatabaseDto).orElse(null))
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }
}
