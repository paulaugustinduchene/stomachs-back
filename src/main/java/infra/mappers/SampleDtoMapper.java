package infra.mappers;

import core.models.Sample;
import infra.databaseDtos.LocationDatabaseDto;
import infra.databaseDtos.SampleDatabaseDto;
import infra.databaseDtos.SpeciesDatabaseDto;
import infra.databaseDtos.StudyDatabaseDto;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class SampleDtoMapper {

	public SampleDtoMapper() {
	}

	public Optional<Sample> dtoToSample(SampleDatabaseDto sampleDatabaseDto){

		if(sampleDatabaseDto == null){
			return Optional.empty();
		}

		Sample sample = new Sample();
		sample.setSampleID(sampleDatabaseDto.getSampleID());
		sample.setSampleDate(sampleDatabaseDto.getSampleDate());
		sample.setSampleLocation(sampleDatabaseDto.getLocationId().getLocationID());
		sample.setSampleStudy(sampleDatabaseDto.getStudyId().getStudyId());
		sample.setSampleSpecies(sampleDatabaseDto.getSpeciesId().getSpeciesID());

		return Optional.of(sample);
	}

	public List<Sample> dtosToSamples(List<SampleDatabaseDto> sampleDatabaseDtos){

		if(sampleDatabaseDtos == null){
			return new ArrayList<>();
		}

		return sampleDatabaseDtos.stream()
				.map(sampleDatabaseDto -> dtoToSample(sampleDatabaseDto).orElse(null))
				.filter(Objects::nonNull)
				.collect(Collectors.toList());
	}

	public Optional<SampleDatabaseDto> sampleToSampleDto(Sample sample){

		SpeciesDatabaseDto speciesDatabaseDto = new SpeciesDatabaseDto();
		speciesDatabaseDto.setSpeciesID(sample.getSampleSpecies());

		LocationDatabaseDto locationDatabaseDto = new LocationDatabaseDto();
		locationDatabaseDto.setLocationID(sample.getSampleLocation());

		StudyDatabaseDto studyDatabaseDto = new StudyDatabaseDto();
		studyDatabaseDto.setStudyId(sample.getSampleStudy());

		SampleDatabaseDto sampleDto = new SampleDatabaseDto();
		sampleDto.setSampleDate(sample.getSampleDate());
		sampleDto.setSpeciesId(speciesDatabaseDto);
		sampleDto.setLocationId(locationDatabaseDto);
		sampleDto.setStudyId(studyDatabaseDto);
		return Optional.of(sampleDto);
	}

}
