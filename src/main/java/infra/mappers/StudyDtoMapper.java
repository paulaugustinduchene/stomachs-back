package infra.mappers;

import core.models.Study;
import infra.databaseDtos.StudyDatabaseDto;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class StudyDtoMapper {

	public StudyDtoMapper() {
	}

	public Optional<Study> dtoToStudy(StudyDatabaseDto studyDatabaseDto) {

		if(studyDatabaseDto == null){
			return Optional.empty();
		}

		Study study = new Study();
		study.setStudyId(studyDatabaseDto.getStudyId());
		study.setStudyName(studyDatabaseDto.getStudyName());
		study.setStudyDoi(studyDatabaseDto.getStudyDoi());
		return Optional.of(study);
	}

	public List<Study> dtosToStudies(List<StudyDatabaseDto> studyDtos) {
		return studyDtos.stream().map(studyDto -> dtoToStudy(studyDto).orElse(null)).collect(Collectors.toList());
	}

	public Optional<StudyDatabaseDto> studyToDto(Study study) {

		if(study == null){
			return Optional.empty();
		}

		StudyDatabaseDto studyDto = new StudyDatabaseDto();
		studyDto.setStudyId(study.getStudyId());
		studyDto.setStudyName(study.getStudyName());
		studyDto.setStudyDoi(study.getStudyDoi());
		return Optional.of(studyDto);
	}

	
}
