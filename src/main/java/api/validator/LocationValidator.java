package api.validator;

import core.models.Location;
import exceptions.CustomMapperException;

public class LocationValidator {

    private static final Double MAX_LONGITUDE = 180.0;
    private static final Double MAX_LATITUDE = 90.0;

    private static final Double MIN_LONGITUDE = -180.0;
    private static final Double MIN_LATITUDE = -90.0;

    public static void validate(Location location) throws CustomMapperException {
        validateNotNull(location);
        validateLongitude(location.getLocationLong());
        validateLatitude(location.getLocationLat());
    }

    private static void validateNotNull(Location location) throws CustomMapperException {
        if(location.getLocationName() == null){
            throw new CustomMapperException("location name can't be null");
        }
    }

    private static void validateLongitude(Double longitude) throws CustomMapperException {

        if(longitude == null){
            throw new CustomMapperException("Longitude can't be null");
        }

        if(longitude < MIN_LONGITUDE || longitude > MAX_LONGITUDE){
            throw new CustomMapperException("Longitude should be in the range o -180 and 180");
        }
    }

    private static void validateLatitude(Double latitude) throws CustomMapperException {
        if(latitude == null){
            throw new CustomMapperException("Latitude can't be null");
        }

        if(latitude < MIN_LATITUDE || latitude > MAX_LATITUDE){
            throw new CustomMapperException("Latitude should be in the range o -90 and 90");
        }
    }


}
