package api.validator;

import core.models.Study;
import exceptions.InvalidDOIException;

import java.util.regex.Pattern;

public class DoiValidator {

    private static final String DOI_REGEX = "^10[.]\\d{4,5}[/](.{1,})";

    public void validate(Study study) throws InvalidDOIException {
        validateDoiPatern(study.getStudyDoi());
    }

    private void validateDoiPatern(String studyDoi) throws InvalidDOIException {
        if(!Pattern.matches(DOI_REGEX, studyDoi)){
            throw new InvalidDOIException("The DOI must have a valid pattern 10.xxxxxx/xxxxxxxxxxxx");
        }
    }
}
