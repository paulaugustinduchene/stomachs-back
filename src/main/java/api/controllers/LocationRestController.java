package api.controllers;

import java.util.List;

import api.validator.LocationValidator;
import core.models.Location;
import core.services.LocationServices;
import exceptions.CustomMapperException;
import exceptions.LocationNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/v1/locations")
public class LocationRestController {

	private LocationServices locationServices;

	public LocationRestController(LocationServices locationServices) {
		this.locationServices = locationServices;
	}

	/***
	 *
	 * @return list of all location registered
	 *
	 * **/
	@CrossOrigin
	@GetMapping
	public ResponseEntity<?> getAllLocations(){
		try {
			List<Location> locations = locationServices.getLocationList();
			return ResponseEntity.ok(locations);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}
	}

	/**
	 *
	 * This method is used to get a location by its id.
	 * 	If the location is found, it is returned in the response.
	 * 	If the location is not found, a 404 error is returned.
	 *
	 * @param id the id of the location
	 *
	 * @return If the location is found, it is returned in the {@link Location}.
	 * 			If the location is not found, a 404 error is returned.
	 *
	 * */
	@GetMapping("/{id}")
	public ResponseEntity<?> getLocationById(@PathVariable("id") Integer id) {
		try {
			Location location = locationServices.getLocationById(id);
			return ResponseEntity.ok(location);
		} catch (LocationNotFoundException cme) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
	}

	/**
	 * This method create a new location In Database
	 *
	 * @param location a {@link Location}
	 *
	 * @return If the object is valid, the location is created and a response with status code 201 (created) is returned.
	 * 			If the Location object is invalid, a response with status code 400 (bad request) is returned.
	 * */
	@PostMapping
	public ResponseEntity<?> createLocation(@RequestBody Location location){
		try {
			LocationValidator.validate(location);
			Location responseLoc = locationServices.createLocation(location);
			return ResponseEntity.status(HttpStatus.CREATED).body(responseLoc);
		} catch (CustomMapperException cme) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		}
	}

   	@PutMapping
	public ResponseEntity<?> updateLocation(@RequestBody Location location) {
		try {
			LocationValidator.validate(location);
			Location updatedLocation = locationServices.updateLocation(location);
			return ResponseEntity.ok(updatedLocation);
		} catch (CustomMapperException cme) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		}
	}

	/**
	 * This method is used to delete a location by id.
	 *
	 * @param id an {@link Integer}
	 *
	 * @return 	If the location is deleted and a 204 status is returned.
	 *			Otherwise, if the location is not found, a 404 error is returned.
	 *
	 */
	@DeleteMapping("/{id}")
	public ResponseEntity<?> deleteLocationById(@PathVariable("id") Integer id) {
		try {
			locationServices.deleteLocationById(id);
			return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
		} catch (LocationNotFoundException e) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
	}
}
