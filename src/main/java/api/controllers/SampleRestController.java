package api.controllers;

import core.models.Sample;
import core.services.SampleServices;
import exceptions.CustomMapperException;
import exceptions.SampleNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("v1/samples")
public class SampleRestController {

    private SampleServices sampleService;

    public SampleRestController(SampleServices sampleServices) {
        this.sampleService = sampleServices;
    }

    @GetMapping
    public ResponseEntity<?> getAllSamples() {
        try {
            List<Sample> samples = sampleService.getAllSamples();
            return ResponseEntity.ok(samples);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getSampleById(@PathVariable("id") Integer id) {
        try {
            Sample sample = sampleService.getSampleById(id);
            return ResponseEntity.ok(sample);
        } catch (SampleNotFoundException snfe) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

   @PostMapping
    public ResponseEntity<?> createSample(@RequestBody Sample sample) {
        try {
            sampleService.createSample(sample);
            return ResponseEntity.ok(sample);
        } catch (CustomMapperException cme) {
            return ResponseEntity.badRequest().body(null);
        }
    }

  //  @PutMapping("/{id}")
    public ResponseEntity<?> updateSample(@PathVariable("id") Long id, @RequestBody Sample sample) {
        try {
            //TODO :
            sampleService.updateSample(id, sample);
            return ResponseEntity.ok(sample);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(null);
        }
    }

   // @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteSample(@PathVariable("id") Long id) {
        try {
            //TODO :
            sampleService.deleteSample(id);
            return ResponseEntity.ok(null);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(null);
        }
    }
}
