package api.controllers;

import java.util.List;

import core.models.Study;
import exceptions.CustomMapperException;
import exceptions.StudyNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import core.services.StudyServices;


@RestController
@RequestMapping("v1/studies")
public class StudyRestController {

	private StudyServices studyServices;

	public StudyRestController(StudyServices studyServices) {
		this.studyServices = studyServices;
	}

	@GetMapping
	public  ResponseEntity<?> getAllSpecies(){
		try {
			List<Study> listStudies = studyServices.listStudies();
			return ResponseEntity.ok(listStudies);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		}

	}

	@GetMapping("/{id}")
	public  ResponseEntity<?> getAllSpecies(@PathVariable("id") Integer id){
		try {
			Study study = studyServices.getStudyById(id);
			return ResponseEntity.ok(study);
		} catch (StudyNotFoundException snfe) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
	}

	@PostMapping
	public ResponseEntity<?> setNewStudy(@RequestBody Study study) {
		try {
			Study responseStudy = studyServices.createStudy(study);
			return ResponseEntity.status(HttpStatus.CREATED).body(responseStudy);
		} catch (CustomMapperException cme) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		}
	}

	@PutMapping("/{id}")
	public ResponseEntity<?> updateStudy(@PathVariable("id") Integer id, @RequestBody Study study) {
		try{
			Study studyUpdated = studyServices.updateStudy(id, study);
			return ResponseEntity.ok(studyUpdated);
		} catch(CustomMapperException e){
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		}
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<?> deleteStudy(@PathVariable("id") Integer id) {
		try {
			studyServices.delete(id);
			return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
		} catch (StudyNotFoundException e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		}
	}
	
}
