package api.controllers;

import core.models.Species;
import exceptions.SpeciesNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import core.services.SpeciesServices;

import java.util.List;

@RestController
@RequestMapping("v1/species")
public class SpeciesRestController {

    private SpeciesServices speciesServices;

    public SpeciesRestController(SpeciesServices speciesServices) {
        this.speciesServices = speciesServices;
    }

    /***
     *
     * @return list of all species
     *
     * **/
    @CrossOrigin
    @GetMapping
    public ResponseEntity<?> getSpecies() {
        List<Species> speciesList = speciesServices.getSpeciesList();
        return ResponseEntity.ok(speciesList);
    }

    @CrossOrigin
    @GetMapping("/{id}")
    public ResponseEntity<?> getSpeciesById(@PathVariable("id") Integer id) {
        try {
            Species species = speciesServices.getSpeciesById(id);
            return ResponseEntity.ok(species);
        } catch (SpeciesNotFoundException snfe) {
            return ResponseEntity.notFound().build();
        }

    }

    /***
     *
     * @return number of species in database
     *
     * **/
    @CrossOrigin
    @GetMapping("/count")
    public ResponseEntity<?> getSpeciesCount(){
        Long numberOfSpecies = speciesServices.speciesCount();
        return ResponseEntity.ok(numberOfSpecies);
    }
}
