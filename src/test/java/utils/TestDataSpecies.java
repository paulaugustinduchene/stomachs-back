package utils;

import core.models.Species;
import infra.databaseDtos.SpeciesDatabaseDto;
import org.springframework.stereotype.Component;

import java.util.List;

@Component("testDataBeanSpecies")
public class TestDataSpecies {

    /************ SpeciesDtos **********/
    public SpeciesDatabaseDto speciesDatabaseDto1 = new SpeciesDatabaseDto();
    public SpeciesDatabaseDto speciesDatabaseDto2 = new SpeciesDatabaseDto();

    /************ SpeciesDtos **********/
    public Species species1 = new Species();
    public Species species2 = new Species();

    /************ Lists **********/
    public List<Species> speciesList = List.of(species1, species2);

    public List<SpeciesDatabaseDto> speciesDatabaseDtoList = List.of(speciesDatabaseDto1, speciesDatabaseDto2);


    {
        /************ SpeciesDtos **********/

        speciesDatabaseDto1.setSpeciesID(0);
        speciesDatabaseDto1.setCommonName("Laysan albatross");
        speciesDatabaseDto1.setKindName("albatross");
        speciesDatabaseDto1.setSpeciesName("Phoebastria immutabilis");
        speciesDatabaseDto1.setFamilyName("Diomedeidae");
        speciesDatabaseDto1.setStatus("least concern");

        speciesDatabaseDto2.setSpeciesID(1);
        speciesDatabaseDto2.setCommonName("Laysan albatross");
        speciesDatabaseDto2.setKindName("albatross");
        speciesDatabaseDto2.setSpeciesName("Phoebastria immutabilis");
        speciesDatabaseDto2.setFamilyName("Diomedeidae");
        speciesDatabaseDto2.setStatus("least concern");

        /************ Species **********/

        species1.setSpeciesID(0);
        species1.setCommonName("Laysan albatross");
        species1.setKindName("albatross");
        species1.setSpeciesName("Phoebastria immutabilis");
        species1.setFamilyName("Diomedeidae");
        species1.setStatus("least concern");

        species2.setSpeciesID(1);
        species2.setCommonName("Laysan albatross");
        species2.setKindName("albatross");
        species2.setSpeciesName("Phoebastria immutabilis");
        species2.setFamilyName("Diomedeidae");
        species2.setStatus("least concern");

    }
}
