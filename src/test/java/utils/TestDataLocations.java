package utils;


import core.models.Location;
import infra.databaseDtos.LocationDatabaseDto;
import org.springframework.stereotype.Component;

@Component("testDataBeanLocation")
public class TestDataLocations {

    public LocationDatabaseDto locationDatabaseDto1 = new LocationDatabaseDto();

    public Location location1 = new Location();

    {
        locationDatabaseDto1.setLocationID(1);
        locationDatabaseDto1.setLocationName("Dunnet Head");
        locationDatabaseDto1.setLocationLat(58.6372);
        locationDatabaseDto1.setLocationLong(-3.3211);

        location1.setLocationID(1);
        location1.setLocationName("Dunnet Head");
        location1.setLocationLat(58.6372);
        location1.setLocationLong(-3.3211);
    }

}
