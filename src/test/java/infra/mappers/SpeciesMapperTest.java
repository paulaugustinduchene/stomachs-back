package infra.mappers;

import core.models.Species;
import org.junit.jupiter.api.*;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import utils.TestDataSpecies;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@DisplayName("Test of SpeciesMapper")
public class SpeciesMapperTest {

    private static AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
            TestDataSpecies.class);
    private TestDataSpecies testDataSpecies = (TestDataSpecies) context.getBean("testDataBeanSpecies");

    private SpeciesDtoMapper speciesDtoMapper = new SpeciesDtoMapper();

    /************* speciesDtoToSpeciesMapper ****************/

    @Test
    @DisplayName("Species DTO mapper should return an empty Optional")
    public void speciesMapper_should_return_emptyOptional(){
        Species species = speciesDtoMapper.specieDtoToSpecies(testDataSpecies.speciesDatabaseDto1).get();
        assertThat(species).isEqualTo(testDataSpecies.species1);
    }

    @Test
    @DisplayName("Species DTO mapper should return Species when given an Optional of Species")
    public void speciesMapper_should_return_Species(){
        assertThat(speciesDtoMapper.specieDtoToSpecies(null)).isEqualTo(Optional.empty());
    }

    /************* speciesDtosToSpeciesMapper ****************/

    @Test
    @DisplayName("Species DTO mapper should return a list of Species")
    public void speciesMapper_should_return_SpeciesList(){
        List<Species> listSpecies = speciesDtoMapper.speciesDtosToSpecies(testDataSpecies.speciesDatabaseDtoList);
        assertThat(listSpecies).isEqualTo(testDataSpecies.speciesList);
        assertThat(listSpecies.size()).isEqualTo(testDataSpecies.speciesList.size());
    }

}
