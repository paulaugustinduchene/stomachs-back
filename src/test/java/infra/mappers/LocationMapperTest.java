package infra.mappers;

import core.models.Location;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import utils.TestDataLocations;


import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

public class LocationMapperTest {

    private static AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
            TestDataLocations.class);

    private TestDataLocations testDataLocations = (TestDataLocations) context.getBean("testDataBeanLocation");

    private LocationDtoMapper locationDtoMapper = new LocationDtoMapper();

    /************* locationDtoToLocationMapper ****************/

    @Test
    public void locationDtoMapper_should_return_location(){
        assertThat(locationDtoMapper.dtoToToLocation(testDataLocations.locationDatabaseDto1).get()).isEqualTo(testDataLocations.location1);
    }

    @Test
    public void locationDtoMapper_is_well_mapped_to_location_coordinates(){
        Location location = locationDtoMapper.dtoToToLocation(testDataLocations.locationDatabaseDto1).get();
        assertThat(location.getLocationLat()).isEqualTo(testDataLocations.locationDatabaseDto1.getLocationLat());
        assertThat(location.getLocationLong()).isEqualTo(testDataLocations.locationDatabaseDto1.getLocationLong());
    }

    @Test
    public void speciesMapper_should_return_emptyOptional(){
        assertThat(locationDtoMapper.dtoToToLocation(null)).isEqualTo(Optional.empty());
    }

    /************* locationsDtosToLocationsMapper ****************/


}
